<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Books');
$this->params['breadcrumbs'][] = $this->title;

limion\bootstraplightbox\BootstrapMediaLightboxAsset::register($this);
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Books'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            'bookName',
            [
                'attribute' => 'previewImageUrl',
                'value' => function($data){
                    return '<a class="lightbox" href="'.$data->fullImageUrl.'"><img src="'.$data->previewImageUrl.'" class="img-thumbnail" /></a>';
                },
                'format' => 'raw'
            ],
            'authorName',
            [
                'attribute' => 'releaseDate',
                'value' => function($data){
                    return Yii::$app->formatter->asDate($data->releaseDate, Yii::$app->params['dateFormat']);
                }
            ],
            [
                'attribute' => 'createDateTime',
                'value' => function($data) {
                    return \yii\timeago\TimeAgo::widget(['timestamp' => $data->createDateTime]);
                },
                'format' => 'raw'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
            ],
           //'template'=>'{view}  {delete}',
        ],
    ]); 
    ?>

</div>
