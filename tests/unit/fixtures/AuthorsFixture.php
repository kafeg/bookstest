<?php
namespace tests\unit\fixtures;

use yii\test\ActiveFixture;

class AuthorsFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Authors';
    
    public function unload()
    {
        $this->resetTable();
        parent::unload();
    }
}
