<?php

return [
    [
        'firstName' => 'Трофим',
        'lastName' => 'Комиссаров',
    ],
    [
        'firstName' => 'Аким',
        'lastName' => 'Овчинников',
    ],
    [
        'firstName' => 'Нестор',
        'lastName' => 'Власов',
    ],
    [
        'firstName' => 'Платон',
        'lastName' => 'Денисов',
    ],
    [
        'firstName' => 'Семён',
        'lastName' => 'Дмитриев',
    ],
    [
        'firstName' => 'Антонин',
        'lastName' => 'Елисеев',
    ],
    [
        'firstName' => 'Дан',
        'lastName' => 'Туров',
    ],
    [
        'firstName' => 'Павел',
        'lastName' => 'Гущин',
    ],
    [
        'firstName' => 'Дан',
        'lastName' => 'Мухин',
    ],
    [
        'firstName' => 'Спартак',
        'lastName' => 'Харитонов',
    ],
    [
        'firstName' => 'Андрей',
        'lastName' => 'Гущин',
    ],
    [
        'firstName' => 'Герман',
        'lastName' => 'Кириллов',
    ],
    [
        'firstName' => 'Станислав',
        'lastName' => 'Кононов',
    ],
    [
        'firstName' => 'Родион',
        'lastName' => 'Яковлев',
    ],
    [
        'firstName' => 'Михаил',
        'lastName' => 'Терентьев',
    ],
    [
        'firstName' => 'Всеволод',
        'lastName' => 'Данилов',
    ],
    [
        'firstName' => 'Алексей',
        'lastName' => 'Тимофеев',
    ],
    [
        'firstName' => 'Виктор',
        'lastName' => 'Зыков',
    ],
    [
        'firstName' => 'Спартак',
        'lastName' => 'Данилов',
    ],
    [
        'firstName' => 'Андрей',
        'lastName' => 'Гордеев',
    ],
    [
        'firstName' => 'Геннадий',
        'lastName' => 'Киселёв',
    ],
    [
        'firstName' => 'Станислав',
        'lastName' => 'Кузнецов',
    ],
    [
        'firstName' => 'Лаврентий',
        'lastName' => 'Копылов',
    ],
    [
        'firstName' => 'Максим',
        'lastName' => 'Савельев',
    ],
    [
        'firstName' => 'Трофим',
        'lastName' => 'Гурьев',
    ],
    [
        'firstName' => 'Ираклий',
        'lastName' => 'Маслов',
    ],
    [
        'firstName' => 'Витольд',
        'lastName' => 'Зыков',
    ],
    [
        'firstName' => 'Вячеслав',
        'lastName' => 'Григорьев',
    ],
    [
        'firstName' => 'Игнатий',
        'lastName' => 'Никонов',
    ],
    [
        'firstName' => 'Аполлон',
        'lastName' => 'Некрасов',
    ],
    [
        'firstName' => 'Николай',
        'lastName' => 'Кузьмин',
    ],
    [
        'firstName' => 'Прохор',
        'lastName' => 'Суханов',
    ],
    [
        'firstName' => 'Гордей',
        'lastName' => 'Белов',
    ],
    [
        'firstName' => 'Владлен',
        'lastName' => 'Копылов',
    ],
    [
        'firstName' => 'Никита',
        'lastName' => 'Баранов',
    ],
    [
        'firstName' => 'Вадим',
        'lastName' => 'Авдеев',
    ],
    [
        'firstName' => 'Роман',
        'lastName' => 'Пахомов',
    ],
    [
        'firstName' => 'Иосиф',
        'lastName' => 'Миронов',
    ],
    [
        'firstName' => 'Валерий',
        'lastName' => 'Громов',
    ],
    [
        'firstName' => 'Евгений',
        'lastName' => 'Соболев',
    ],
    [
        'firstName' => 'Глеб',
        'lastName' => 'Фомин',
    ],
    [
        'firstName' => 'Донат',
        'lastName' => 'Селезнёв',
    ],
    [
        'firstName' => 'Ростислав',
        'lastName' => 'Марков',
    ],
    [
        'firstName' => 'Ян',
        'lastName' => 'Шаров',
    ],
    [
        'firstName' => 'Родион',
        'lastName' => 'Фролов',
    ],
    [
        'firstName' => 'Артём',
        'lastName' => 'Лукин',
    ],
    [
        'firstName' => 'Фёдор',
        'lastName' => 'Симонов',
    ],
    [
        'firstName' => 'Рафаил',
        'lastName' => 'Щукин',
    ],
    [
        'firstName' => 'Спартак',
        'lastName' => 'Богданов',
    ],
    [
        'firstName' => 'Леонид',
        'lastName' => 'Носов',
    ],
];
