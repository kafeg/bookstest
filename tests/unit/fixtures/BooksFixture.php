<?php
namespace tests\unit\fixtures;

use yii\test\ActiveFixture;

class BooksFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Books';
    
    public function unload()
    {
        $this->resetTable();
        parent::unload();
    }
}
