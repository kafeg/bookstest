<?php
/**
* @var $faker \Faker\Generator
* @var $index integer
*/
return [
    //'id'=> ($index+1),
    'bookName'=> $faker->sentence($nbWords = 3, $variableNbWords = true),
    //'bookName'=> $faker->company(),
    'createDateTime'=> date("Y-m-d H:i:s", $faker->unixTime()),
    'updateDateTime'=> date("Y-m-d H:i:s", $faker->unixTime()),
    'previewImageUrl'=> $faker->imageUrl($width = 320, $height = 240),
    'fullImageUrl'=> $faker->imageUrl($width = 640, $height = 480),
    'releaseDate'=> $faker->date(),
    'authorId'=> $faker->numberBetween($min = 1, $max = 50),
];
