<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Books;

/**
 * BooksSearch represents the model behind the search form about `app\models\Books`.
 */
class BooksSearch extends Books
{
    public $author;
    public $authorName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'authorId'], 'integer'],
            [['bookName', 'createDateTime', 'updateDateTime', 'previewImageUrl', 'fullImageUrl', 'releaseDate', 'author'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Books::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'createDateTime' => $this->createDateTime,
            'updateDateTime' => $this->updateDateTime,
            'releaseDate' => $this->releaseDate,
            'authorId' => $this->authorId,
        ]);

        $query->andFilterWhere(['like', 'bookName', $this->bookName])
            ->andFilterWhere(['like', 'previewImageUrl', $this->previewImageUrl])
            ->andFilterWhere(['like', 'fullImageUrl', $this->fullImageUrl]);

        return $dataProvider;
    }
}
