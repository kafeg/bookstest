<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $bookName
 * @property string $createDateTime
 * @property string $updateDateTime
 * @property string $previewImageUrl
 * @property string $fullImageUrl
 * @property string $releaseDate
 * @property integer $authorId
 *
 * @property Authors $author
 */
class Books extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookName', 'previewImageUrl', 'fullImageUrl'], 'required'],
            [['createDateTime', 'updateDateTime', 'releaseDate'], 'safe'],
            [['authorId'], 'integer'],
            [['bookName', 'previewImageUrl', 'fullImageUrl'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Book ID'),
            'bookName' => Yii::t('app', 'Book Name'),
            'createDateTime' => Yii::t('app', 'Record create Date and Time'),
            'updateDateTime' => Yii::t('app', 'Record update Date and Time'),
            'previewImageUrl' => Yii::t('app', 'Preview image'),
            'fullImageUrl' => Yii::t('app', 'Full image'),
            'releaseDate' => Yii::t('app', 'Date of release'),
            'authorId' => Yii::t('app', 'Author'),
            'authorName' => Yii::t('app', 'Author Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'authorId']);
    }
    
    public function getAuthorName()
    {
        $model = Authors::find()->where(['id' => $this->authorId])->one();
        return $model->firstName . ' ' . $model->lastName;
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->createDateTime = date("Y-m-d H:i:s", time ());
        } else {
            $this->updateDateTime = date("Y-m-d H:i:s", time ());
        }
    
        return parent::beforeSave($insert);
    }
}
