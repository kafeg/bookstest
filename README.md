Books
=====

Test project developed via docs/specification.pdf

INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

Project developed with PHP5 and MySql 5.5.38 on Ubuntu 14.04.1 and Apache 2.4.7.

You can then install this project template using the following command:

~~~
sudo apt-get install php5-intl
php composer.phar global require "fxp/composer-asset-plugin:~1.0.0"
mkdir ~/books
cd ~/books/
git clone git@bitbucket.org:kafeg/bookstest.git html
cd html
sudo cp docs/books.local.conf /etc/apache2/sites-available/books.local.conf
sudo a2ensite books.local
sudo a2enmod rewrite
sudo service apache2 reload
composer update
~~~

Now you must setup databse connection via `config/db.php`. After this, do:

~~~
cd ~/books/html
php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations
php yii migrate/up --migrationPath=@yii/rbac/migrations
php yii migrate/up
php yii fixture/generate authors --count=50 --language="ru_RU"
php yii fixture/generate books --count=2000 --language="ru_RU"
php yii fixture/load Authors
php yii fixture/load Books
~~~

Now you should be able to access the application through the following URL.

~~~
http://books.local
~~~
