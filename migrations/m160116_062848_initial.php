<?php

use yii\db\Schema;
use yii\db\Migration;

class m160116_062848_initial extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%authors}}', [
            'id' => Schema::TYPE_PK . ' COMMENT "Author ID"',
            'firstName' => Schema::TYPE_STRING . ' NOT NULL COMMENT "First Name"',
            'lastName' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Last Name"',
        ], $tableOptions);
        
        $this->createTable('{{%books}}', [
            'id' => Schema::TYPE_PK . ' COMMENT "Book ID"',
            'bookName' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Book Name"',
            'createDateTime' => Schema::TYPE_DATETIME . ' COMMENT "Record create Date and Time"',
            'updateDateTime' => Schema::TYPE_DATETIME . ' COMMENT "Record update Date and Time"',
            'previewImageUrl' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Preview image"',
            'fullImageUrl' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Full image"',
            'releaseDate' => Schema::TYPE_DATE . ' COMMENT "Date of release"',
            'authorId' => Schema::TYPE_INTEGER . ' COMMENT "Author"',
        ], $tableOptions);
        
        $this->createIndex('FK_books_authors', '{{%books}}', 'authorId');
        $this->addForeignKey(
            'FK_books_authors', '{{%books}}', 'authorId', '{{%authors}}', 'id', 'SET NULL', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%books}}');
        $this->dropTable('{{%authors}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
